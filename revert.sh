#!/bin/bash
SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

virsh net-destroy external
#virsh net-destroy internal

cp ${SCRIPTPATH}/networks/external.xml /etc/libvirt/qemu/networks/external.xml
#cp ${SCRIPTPATH}/networks/internal.xml /etc/libvirt/qemu/networks/internal.xml
virsh net-undefine external
#virsh net-undefine internal

#rm /etc/libvirt/qemu/networks/external.xml
#rm /etc/libvirt/qemu/networks/internal.xml
#rm /etc/libvirt/qemu/networks/management.xml

virsh destroy salt1
virsh undefine salt1
virsh destroy salt2
virsh undefine salt2

#rm -rf /var/lib/libvirt/images/vm1
#rm -rf /var/lib/libvirt/images/vm2
virsh pool-destroy salt1
virsh pool-undefine salt1
virsh pool-destroy salt2
virsh pool-undefine salt2
