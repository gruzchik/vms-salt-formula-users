#!/bin/bash

SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# enable config file
source ${SCRIPTPATH}/config

echo vm1=${VM1_NAME}
echo vm2=${VM2_NAME}

#create xml external network and start net
[ ! -d ${SCRIPTPATH}/networks ] && mkdir -p ${SCRIPTPATH}/networks
MAC=52:54:00:`(date; cat /proc/interrupts) | md5sum | sed -r 's/^(.{6}).*$/\1/; s/([0-9a-f]{2})/\1:/g; s/:$//;'`
cat <<EOF > ${SCRIPTPATH}/networks/external.xml
<network>
    <name>${EXTERNAL_NET_NAME}</name>
    <forward mode='nat'>
      <nat>
        <port start='1024' end='65535'/>
      </nat>
    </forward>
    <ip address='${EXTERNAL_NET_HOST_IP}' netmask='${EXTERNAL_NET_MASK}'>
      <dhcp>
        <range start='${EXTERNAL_NET}.2' end='${EXTERNAL_NET}.254' />
        <host mac='${MAC}' name='${VM1_NAME}' ip='${VM1_EXTERNAL_IP}'/>
      </dhcp>
    </ip>
  </network>
EOF

virsh net-define ${SCRIPTPATH}/networks/external.xml
virsh net-autostart ${EXTERNAL_NET_NAME}
virsh net-start ${EXTERNAL_NET_NAME}

#create xml internal network and start net
cat <<EOF > ${SCRIPTPATH}/networks/internal.xml
<network>
    <name>${INTERNAL_NET_NAME}</name>
    <forward mode='nat'>
      <nat>
        <port start='1024' end='65535'/>
      </nat>
  </network>
EOF

virsh net-define ${SCRIPTPATH}/networks/internal.xml
virsh net-autostart ${INTERNAL_NET_NAME}
virsh net-start ${INTERNAL_NET_NAME}

# prepare disk for vm1 and vm2
VM1_HDD_PATH=$(dirname ${VM1_HDD})
[ ! -d ${VM1_HDD_PATH} ] && mkdir -p ${VM1_HDD_PATH}
VM2_HDD_PATH=$(dirname ${VM2_HDD})
[ ! -d ${VM2_HDD_PATH} ] && mkdir -p ${VM2_HDD_PATH}

if [ ! -x "$(command -v wget)" ]; then
	apt-get install -y wget
fi

if [ ! -f /var/lib/libvirt/images/bionic-server-cloudimg-amd64.qcow2 ]; then
	wget -O /var/lib/libvirt/images/bionic-server-cloudimg-amd64.qcow2 ${VM1_BASE_IMAGE}
fi

if [ ! -f "/var/lib/libvirt/images/CentOS-7-x86_64-GenericCloud-1503.qcow2" ]; then
        wget -O "/var/lib/libvirt/images/CentOS-7-x86_64-GenericCloud-1503.qcow2" ${VM2_BASE_IMAGE}
fi

cp -f /var/lib/libvirt/images/bionic-server-cloudimg-amd64.qcow2 ${VM1_HDD}
cp -f /var/lib/libvirt/images/CentOS-7-x86_64-GenericCloud-1503.qcow2 ${VM2_HDD}

# starting configuration for salt1
#ssh-keygen -t rsa -f $HOME/.ssh/id_rsa3 -q -N ""
[ ! -d ${SCRIPTPATH}/config-drives/salt1-config ] && mkdir -p ${SCRIPTPATH}/config-drives/salt1-config
# meta-data vm1
cat <<EOF > ${SCRIPTPATH}/config-drives/salt1-config/meta-data
instance-id: siid-abcdefg
hostname: ${VM1_NAME}
local-hostname: ${VM1_HOSTNAME}
network-interfaces: |
  auto ${VM1_EXTERNAL_IF}
  allow-hotplug ${VM1_EXTERNAL_IF}
  iface ${VM1_EXTERNAL_IF} inet dhcp
EOF

# user-data salt1
cat <<EOF > ${SCRIPTPATH}/config-drives/salt1-config/user-data
#cloud-config
password: 123
chpasswd: { expire: False }
ssh_pwauth: True
ssh_authorized_keys:
  - $(cat  $SSH_PUB_KEY)
runcmd:
  - echo "1" > /proc/sys/net/ipv4/ip_forward
  - iptables -t nat -I POSTROUTING -o ${VM1_EXTERNAL_IF} -j MASQUERADE
EOF

# starting configuration for salt2
[ ! -d ${SCRIPTPATH}/config-drives/salt2-config ] && mkdir -p ${SCRIPTPATH}/config-drives/salt2-config
# meta-data vm2
cat <<EOF > ${SCRIPTPATH}/config-drives/salt2-config/meta-data
instance-id: siid2-abcdefg
hostname: ${VM2_NAME}
local-hostname: ${VM2_HOSTNAME}
network-interfaces: |
  auto ${VM2_INTERNAL_IF}
  iface ${VM2_INTERNAL_IF} inet static
  address ${VM2_INTERNAL_IP}
  netmask ${INTERNAL_NET_MASK}
  gateway ${EXTERNAL_NET_HOST_IP}
  dns-nameservers ${VM_DNS}
EOF

# user-data salt2
cat <<EOF > ${SCRIPTPATH}/config-drives/salt2-config/user-data
#cloud-config
password: 123
chpasswd: { expire: False }
ssh_pwauth: True
ssh_authorized_keys:
  - $(cat  $SSH_PUB_KEY)
EOF

# create iso for vm1 and vm2
mkisofs -o "${VM1_CONFIG_ISO}" -V cidata -r -J ${SCRIPTPATH}/config-drives/salt1-config
mkisofs -o "${VM2_CONFIG_ISO}" -V cidata -r -J ${SCRIPTPATH}/config-drives/salt2-config

# create vm1
echo 'create salt1...'
virt-install \
--connect qemu:///system \
--name ${VM1_NAME} \
--ram ${VM1_MB_RAM} --vcpus=${VM1_NUM_CPU} --hvm \
--os-type=linux --os-variant=generic \
--disk path=${VM1_HDD},format=qcow2,bus=virtio,cache=none \
--disk path=${VM1_CONFIG_ISO},device=cdrom \
--network network=${EXTERNAL_NET_NAME},mac=${MAC} \
--network network=${INTERNAL_NET_NAME} \
--graphics vnc,port=-1 \
--noautoconsole --quiet --virt-type "${VM_VIRT_TYPE}" --import

# create vm2
echo 'create salt2...'
virt-install \
--connect qemu:///system \
--name ${VM2_NAME} \
--ram ${VM2_MB_RAM} --vcpus=${VM2_NUM_CPU} --hvm \
--os-type=linux --os-variant=generic \
--disk path=${VM2_HDD},format=qcow2,bus=virtio,cache=none \
--disk path=${VM2_CONFIG_ISO},device=cdrom \
--network network=${EXTERNAL_NET_NAME} \
--graphics vnc,port=-1 \
--noautoconsole --quiet --virt-type "${VM_VIRT_TYPE}" --import
